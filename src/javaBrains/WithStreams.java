package javaBrains;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mohit on 4/10/18.
 */
public class WithStreams {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carroll", 42),
                new Person("Thomas", "Carlyle", 51),
                new Person("Charlotte", "Bronte", 45),
                new Person("Matthew", "Arnold", 60)
        );
        //Step 1:Sort list by last name
        people.stream()
                .sorted((p1,p2)->p1.getLastName().compareTo(p2.getLastName()))
                .forEach(p-> System.out.println(p));
        //Step 2:Create a method that prints all elements in the list
        System.out.println("===================================================================");
        people.stream()
                .forEach(p-> System.out.println(p));
        //Step 3: Create a method that prints all the people who have last name beginning with C
        System.out.println("===================================================================");
        people.stream()
                .filter(p->p.getLastName().startsWith("C"))
                .forEach(p-> System.out.println(p));

    }
}
