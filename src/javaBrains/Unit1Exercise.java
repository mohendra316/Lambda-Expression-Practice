package javaBrains;

import java.util.*;

/**
 * Created by mohit on 4/6/18.
 */
public class Unit1Exercise {

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carroll", 42),
                new Person("Thomas", "Carlyle", 51),
                new Person("Charlotte", "Bronte", 45),
                new Person("Matthew", "Arnold", 60)
        );
        Unit1Exercise unit1exercise = new Unit1Exercise();
        //Step 1:Sort list by last name
        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getLastName().compareTo(o2.getLastName());
            }
        });

        //Step 2:Create a method that prints all elements in the list
        unit1exercise.showAllPeople(people);
        System.out.println("-------------------------------------");
        //Step 3: Create a method that prints all the people who have last name beginning with C
        printConditionally(people, new Condition() {
            @Override
            public boolean test(Person p) {
                return p.getLastName().startsWith("C");
            }
        });

    }

    public static void printConditionally(List<Person> people, Condition condition) {
        for (Person p : people) {
            if (condition.test(p)) {
                System.out.println(p);
            }
        }
    }

    public void showAllPeople(List<Person> people) {
        for (Person person : people
                ) {
            System.out.println(person);
        }
    }
}

interface Condition {
    boolean test(Person p);
}