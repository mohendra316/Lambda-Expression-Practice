package javaBrains;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by mohit on 4/9/18.
 */
public class WithFunctionalInterface {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carroll", 42),
                new Person("Thomas", "Carlyle", 51),
                new Person("Charlotte", "Bronte", 45),
                new Person("Matthew", "Arnold", 60)
        );
        Unit1ExercisewithLambda unit1exercise = new Unit1ExercisewithLambda();
        //Step 1:Sort list by last name
        Collections.sort(people,(o1, o2)-> o1.getLastName().compareTo(o2.getLastName()));

        //Step 2:Create a method that prints all elements in the list
        unit1exercise.showAllPeople(people);
        System.out.println("-------------------------------------");
        //Step 3: Create a method that prints all the people who have last name beginning with C
        printConditionally(people,p-> {
            return p.getLastName().startsWith("C");
        });System.out.println("-------------------------------------");
        System.out.println("-------------------------------------");
        printConditionally(people, p->true);
        System.out.println("-------------------------------------");
        printConditionally(people, new Predicate<Person>() {
            @Override
            public boolean test(Person person) {
               return person.getLastName().startsWith("D");
            }
        });

    }

//    public static void printConditionally(List<Person> people, Predicate<Person> predicate) {
//        for (Person p : people) {
//            if (predicate.test(p)) {
//                System.out.println(p);
//            }
//        }
//    }

    public static void printConditionally(List<Person> people, Predicate<Person> predicate) {
        for (Person p : people) {
            if (predicate.test(p)) {
                System.out.println(p);
            }
        }
    }

    public void showAllPeople(List<Person> people) {
        for (Person person : people
                ) {
            System.out.println(person);
        }
    }
}
