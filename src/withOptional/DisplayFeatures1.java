package withOptional;

import java.util.Optional;

/**
 * Created by mohit on 4/13/18.
 */
public class DisplayFeatures1 {
    private String size;
    private Optional<ScreenResolution1> resolution;

    public DisplayFeatures1(String size, Optional<ScreenResolution1> resolution) {
        this.size = size;
        this.resolution = resolution;
    }

    public String getSize() {
        return size;
    }

    public Optional<ScreenResolution1> getResolution() {
        return resolution;
    }
}
