package withOptional;



import java.util.Optional;

/**
 * Created by mohit on 4/13/18.
 */
public class MobileService1 {
    public Integer getMobileScreenWidth(Optional<Mobile1> mobile){
        return mobile.flatMap(Mobile1::getDisplayFeatures)
                .flatMap(DisplayFeatures1::getResolution)
                .map(ScreenResolution1::getWidth)
                .orElse(0);
    }
}
