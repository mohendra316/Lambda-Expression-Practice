package withOptional;

/**
 * Created by mohit on 4/13/18.
 */
public class ScreenResolution1 {
    private int width;
    private int height;

    public ScreenResolution1(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
