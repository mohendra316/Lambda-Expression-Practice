package withoutOptional;

/**
 * Created by mohit on 4/13/18.
 */
public class ScreenResolution {
    private int width;
    private int height;

    public ScreenResolution(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
