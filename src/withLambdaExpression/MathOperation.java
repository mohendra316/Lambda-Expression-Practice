package withLambdaExpression;

/**
 * Created by mohit on 4/6/18.
 */
public interface MathOperation {
    int operation(int a,int b);
}
