package withoutLambda;

/**
 * Created by mohit on 4/6/18.
 */
public class MultiplyOperation implements MathOperation{
    @Override
    public int operation(int a, int b) {
        return a*b;
    }
}
