package withoutLambda;

/**
 * Created by mohit on 4/6/18.
 */
public class Java8Tester{
    public static void performOperation(int a, int b, MathOperation mathOperation){
        System.out.println(mathOperation.operation(a, b));
    }


    public static void main(String[] args) {
        AddOperation addOperation = new AddOperation();
        SubtractOperation subtractOperation = new SubtractOperation();
        MultiplyOperation multiplyOperation = new MultiplyOperation();
        performOperation(3,5,addOperation);
        performOperation(3,5,subtractOperation);
        performOperation(3,5,multiplyOperation);
    }
}
